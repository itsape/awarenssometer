## -*- coding: utf-8 -*-
The *Individual IT-Security Awareness* (**IITSA**) is the average probability
of the complementary event of an artifact reaction without a report:
$1-P(\neg pos \, \cap \, neg)$. As a probability, its value ranges from 0 to 1.
The higher the value, the better the Individual IT-Security Awareness.
