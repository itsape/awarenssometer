## -*- coding: utf-8 -*-
Die *Individuelle IT-Security Awareness* (**IITSA**) ist die mittlere
Wahrscheinlichkeit für das Gegenereignis einer Artefaktreaktion ohne Report:
$1-P(\neg pos \, \cap \, neg)$. Als Wahrscheinlichkeit liegt dieser Wert
zwischen 0 und 1. Ein höherer Wert bedeutet bessere Individuelle IT-Security
Awareness.
