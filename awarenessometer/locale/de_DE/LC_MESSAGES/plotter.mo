��    
      l      �       �      �   (        4     @     H     _  %   }  %   �  T   �         6  4   S     �     �     �     �  %   �  %   �  U        
                           	              on Artifacts of Phase {} Average Individual IT-Security Awareness Correlation Density Group A (intervention) Group B ($\neg$ intervention) Phase 1 $\bf{⟵}$ $\bf{⟶}$ Phase 2 positive $\bf{and}$
negative reaction ratio of participants with
positive reaction $\bf{⟵}$ $\bf{⟶}$ negative reaction Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  für Artefakte der Phase {} Durchschnittliche Individuelle IT-Security Awareness Korrelation Dichte Gruppe A (Intervention) Gruppe B ($\neg$ intervention) Phase 1 $\bf{⟵}$ $\bf{⟶}$ Phase 2 positive $\bf{und}$
negative reaktion Anteil der Teilnehmer mit
positiver reaktion $\bf{⟵}$ $\bf{⟶}$ negativer reaktion 