import importlib
import os
import unittest

import yaml
import pandas as pd
import numpy as np
from scipy.stats import pearsonr

import awarenessometer.pre_post_study


_DATA_PATH = os.path.join(os.path.dirname(__file__), 'test_data')
with open(os.path.join(_DATA_PATH, 'series_pre_data.yml'), 'r') as stream:
    SERIES_PRE_DATA = yaml.safe_load(stream)
with open(os.path.join(_DATA_PATH, 'series_post_data.yml'), 'r') as stream:
    SERIES_POST_DATA = yaml.safe_load(stream)
SUBJECTS_DF = pd.read_csv(os.path.join(_DATA_PATH, 'pre_post_subjects.csv'), keep_default_na=False, na_values=[])
REPORT_PRE = pd.read_csv(os.path.join(_DATA_PATH, 'extended_report_pre.csv'))
REPORT_POST = pd.read_csv(os.path.join(_DATA_PATH, 'extended_report_post.csv'))
IMG_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), 'test_data', 'img'))
GOOD_EVAL_MATRIX = np.loadtxt(os.path.join(_DATA_PATH, 'pre_post_good_eval_matrix.csv'))
POSITIVE_ACTION_MATRIX = np.loadtxt(os.path.join(_DATA_PATH, 'pre_post_positive_action_matrix.csv'))
SERIES_DATA = {
    'pre': SERIES_PRE_DATA,
    'post': SERIES_POST_DATA
}
REPORTS = {
    'pre': REPORT_PRE,
    'post': REPORT_POST
}
SERIES_INFO = {
    'series_data': SERIES_DATA,
    'subject_df': SUBJECTS_DF,
    'reports': REPORTS,
    'matrices': {
        'good_evals': GOOD_EVAL_MATRIX,
        'positive_actions': POSITIVE_ACTION_MATRIX
    }
}


def img_path(filename):
    return os.path.join('img', filename)


def test_create_tokens():
    series_pre_info = {
        'title': 'PreTestSeries',
        'start': '2020-03-06 12:12:51',
        'end': '2020-03-06 12:22:51',
        '#participants': 10,
        '#artifacts': 2
    }
    series_post_info = {
        'title': 'PostTestSeries',
        'start': '2020-03-06 12:12:51',
        'end': '2020-03-06 12:22:51',
        '#participants': 9,
        '#artifacts': 2
    }
    participation_stats = {
        '#pre_participants': 10,
        '#post_participants': 9,
        '#pre_x_post': 8,
        '#pre_only': '2 (20.00%)',
        '#post_only': '1 (11.11%)',
        '#schooled': '4 (36.36%)',
        '#schooled_pre': '4 (40.00%)',
        '#schooled_post': '4 (44.44%)',
        '#schooled_pre_post': '4 (50.00%)',
        '#missed_pre': '1 (10.00%)',
        '#armed_pre': '9 (90.00%)',
        '#pre_artifacts': '1.70 0.67 0.00 2.00',
        '#missed_post': '2 (22.22%)',
        '#armed_post': '7 (77.78%)',
        '#post_artifacts': '1.44 0.88 0.00 2.00'
    }
    reaction_stats = {
        'p_nopos_pre': '41.18%',
        'p_nopos_post': '61.54%',
        'p_nopos': '50.00%',
        'p_pos_neg_pre': '47.06%',
        'p_pos_neg_post': '38.46%',
        'p_pos_neg': '43.33%',
        'p_pos_noneg_pre': '11.76%',
        'p_pos_noneg_post': '0.00%',
        'p_pos_noneg': '6.67%',
        'p_nopos_neg_pre': '23.53%',
        'p_nopos_neg_post': '30.77%',
        'p_nopos_neg': '26.67%',
        'p_nopos_noneg_pre': '17.65%',
        'p_nopos_noneg_post': '30.77%',
        'p_nopos_noneg': '23.33%',
        'iv_p_nopos_pre': '12.50%',
        'iv_p_nopos_post': '50.00%',
        'iv_p_nopos': '31.25%',
        'noiv_p_nopos_pre': '66.67%',
        'noiv_p_nopos_post': '80.00%',
        'noiv_p_nopos': '71.43%',
        'iv_p_neg_pre': '87.50%',
        'iv_p_neg_post': '75.00%',
        'iv_p_neg': '81.25%',
        'noiv_p_neg_pre': '55.56%',
        'noiv_p_neg_post': '60.00%',
        'noiv_p_neg': '57.14%',
        'iv_p_nopos_neg_pre': '12.50%',
        'iv_p_nopos_neg_post': '25.00%',
        'iv_p_nopos_neg': '18.75%',
        'noiv_p_nopos_neg_pre': '33.33%',
        'noiv_p_nopos_neg_post': '40.00%',
        'noiv_p_nopos_neg': '35.71%',
        'iv_citsa_pre': '1.0000',
        'iv_citsa_post': '1.0000',
        'iv_citsa': '1.0000',
        'noiv_citsa_pre': '0.9999',
        'noiv_citsa_post': '0.9998',
        'noiv_citsa': '0.9999',
        'citsa_increase_pre': '0.01%',
        'citsa_increase_post': '0.01%',
        'citsa_increase': '0.01%',
        'iv_iitsa_pre': '0.8750',
        'iv_iitsa_post': '0.7500',
        'iv_iitsa': '0.8125',
        'noiv_iitsa_pre': '0.6667',
        'noiv_iitsa_post': '0.6000',
        'noiv_iitsa': '0.6429',
        'iitsa_increase_pre': '31.25%',
        'iitsa_increase_post': '25.00%',
        'iitsa_increase': '26.39%',
        'good_evals_pre_mean': '0.78',
        'good_evals_pre_std': '0.36',
        'good_evals_pre_min': '0.00',
        'good_evals_pre_max': '1.00',
        'good_evals_post_mean': '0.71',
        'good_evals_post_std': '0.39',
        'good_evals_post_min': '0.00',
        'good_evals_post_max': '1.00',
        'good_evals_mean': '0.78',
        'good_evals_std': '0.28',
        'good_evals_min': '0.25',
        'good_evals_max': '1.00',
        'bad_evals_pre_mean': '0.22',
        'bad_evals_pre_std': '0.36',
        'bad_evals_pre_min': '0.00',
        'bad_evals_pre_max': '1.00',
        'bad_evals_post_mean': '0.29',
        'bad_evals_post_std': '0.39',
        'bad_evals_post_min': '0.00',
        'bad_evals_post_max': '1.00',
        'bad_evals_mean': '0.22',
        'bad_evals_std': '0.28',
        'bad_evals_min': '0.00',
        'bad_evals_max': '0.75',
        'good_evals_iv_pre_mean': '0.88',
        'good_evals_iv_pre_std': '0.25',
        'good_evals_iv_pre_min': '0.50',
        'good_evals_iv_pre_max': '1.00',
        'good_evals_iv_post_mean': '0.75',
        'good_evals_iv_post_std': '0.29',
        'good_evals_iv_post_min': '0.50',
        'good_evals_iv_post_max': '1.00',
        'good_evals_iv_mean': '0.81',
        'good_evals_iv_std': '0.12',
        'good_evals_iv_min': '0.75',
        'good_evals_iv_max': '1.00',
        'bad_evals_iv_pre_mean': '0.12',
        'bad_evals_iv_pre_std': '0.25',
        'bad_evals_iv_pre_min': '0.00',
        'bad_evals_iv_pre_max': '0.50',
        'bad_evals_iv_post_mean': '0.25',
        'bad_evals_iv_post_std': '0.29',
        'bad_evals_iv_post_min': '0.00',
        'bad_evals_iv_post_max': '0.50',
        'bad_evals_iv_mean': '0.19',
        'bad_evals_iv_std': '0.12',
        'bad_evals_iv_min': '0.00',
        'bad_evals_iv_max': '0.25',
        'good_evals_noiv_pre_mean': '0.70',
        'good_evals_noiv_pre_std': '0.45',
        'good_evals_noiv_pre_min': '0.00',
        'good_evals_noiv_pre_max': '1.00',
        'good_evals_noiv_post_mean': '0.67',
        'good_evals_noiv_post_std': '0.58',
        'good_evals_noiv_post_min': '0.00',
        'good_evals_noiv_post_max': '1.00',
        'good_evals_noiv_mean': '0.76',
        'good_evals_noiv_std': '0.37',
        'good_evals_noiv_min': '0.25',
        'good_evals_noiv_max': '1.00',
        'bad_evals_noiv_pre_mean': '0.30',
        'bad_evals_noiv_pre_std': '0.45',
        'bad_evals_noiv_pre_min': '0.00',
        'bad_evals_noiv_pre_max': '1.00',
        'bad_evals_noiv_post_mean': '0.33',
        'bad_evals_noiv_post_std': '0.58',
        'bad_evals_noiv_post_min': '0.00',
        'bad_evals_noiv_post_max': '1.00',
        'bad_evals_noiv_mean': '0.24',
        'bad_evals_noiv_std': '0.37',
        'bad_evals_noiv_min': '0.00',
        'bad_evals_noiv_max': '0.75'
    }
    artifacts_pre = [
        {'recipe_id': 'PreRecipe1', 'images': [img_path('PreRecipe1_1.png'), img_path('PreRecipe1_2.png')]},
        {'recipe_id': 'PreRecipe2', 'images': []}
    ]
    artifacts_post = [
        {'recipe_id': 'PostRecipe1', 'images': []},
        {'recipe_id': 'PostRecipe2', 'images': [img_path('PostRecipe2_1.png'), img_path('PostRecipe2_2.jpg')]}
    ]
    cronbachs_alpha = {
        'good_eval': '0.46',
        'positive_action': '0.40'
    }
    expected_tokens = {
        'series_pre_info': series_pre_info,
        'series_post_info': series_post_info,
        'participation_stats': participation_stats,
        'reaction_stats': reaction_stats,
        'artifacts_pre': artifacts_pre,
        'artifacts_post': artifacts_post,
        'cronbachs_alpha': cronbachs_alpha
    }

    study = awarenessometer.pre_post_study.PrePostStudy(10000,
                                                       SERIES_INFO,
                                                       IMG_PATH)
    tokens = study.create_tokens()

    assert tokens == expected_tokens


class TestPlotCreation:
    def test_create_good_eval_correlations(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.pre_post_study.Plotter, 'distplot')
        af1_vals = [22/30, 1, 22/30, 0, 0, 0, 1, 1, 1, 1, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30]
        af2_vals = [22/30, 1, 1, 0, 1, 1, 1, 1, 1, 1, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30]
        af3_vals = [22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 0, 1, 0, 0, 1, 1]
        af4_vals = [22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 22/30, 1, 0, 1, 1, 1, 1, 1]
        correlations = [pearsonr(af1_vals, af2_vals)[0], pearsonr(af1_vals, af3_vals)[0], pearsonr(af1_vals, af4_vals)[0],
                        pearsonr(af2_vals, af3_vals)[0], pearsonr(af2_vals, af4_vals)[0], pearsonr(af3_vals, af4_vals)[0]]

        plt_path = os.path.join(IMG_PATH, 'fig_good_eval_correlation_distplot.pdf')

        study = awarenessometer.pre_post_study.PrePostStudy(10000, SERIES_INFO, IMG_PATH)
        study._plot_good_eval_correlations()

        mock_plotter.assert_called_with(correlations, plt_path)

    def test_create_positive_action_correlations(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.pre_post_study.Plotter, 'distplot')
        af1_vals = [1/2, 0, 1/2, 0, 0, 0, 1, 1, 1, 1, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2]
        af2_vals = [1/2, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2]
        af3_vals = [1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 0, 1, 0, 0, 1, 0]
        af4_vals = [1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1/2, 1, 0, 1, 0, 1, 0, 0]
        correlations = [pearsonr(af1_vals, af2_vals)[0], pearsonr(af1_vals, af3_vals)[0], pearsonr(af1_vals, af4_vals)[0],
                        pearsonr(af2_vals, af3_vals)[0], pearsonr(af2_vals, af4_vals)[0], pearsonr(af3_vals, af4_vals)[0]]

        plt_path = os.path.join(IMG_PATH, 'fig_positive_action_correlation_distplot.pdf')

        study = awarenessometer.pre_post_study.PrePostStudy(10000, SERIES_INFO, IMG_PATH)
        study._plot_positive_action_correlations()

        mock_plotter.assert_called_with(correlations, plt_path)

    def test_create_reactions_plot(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.pre_post_study.Plotter, 'reactions_by_artifact_relative')

        artifacts = ['PreRecipe1', 'PreRecipe2', 'PostRecipe1', 'PostRecipe2']
        no_reactions = {
            'good': [-(4 / 8), -(6 / 9), -(2 / 6), -(3 / 7)],
            'bad': [6 / 8, 6 / 9, 5 / 6, 4 / 7],
            'good_and_bad': [3 / 8, 5 / 9, 2 / 6, 3 / 7]
        }
        phase_1_artifact_amount = 2
        plt_path = os.path.join(IMG_PATH, 'fig_reactions_by_artifact_relative.pdf')

        study = awarenessometer.pre_post_study.PrePostStudy(10000, SERIES_INFO, IMG_PATH)
        study._plot_reactions_by_artifact_relative()

        mock_plotter.assert_called_with(artifacts, no_reactions, phase_1_artifact_amount, plt_path)

    def test_create_pre_kde(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.pre_post_study.Plotter, 'iitsa_kde')

        iitsas = [pd.Series([1, 2, 2, 2]), pd.Series([1, 0, 2, 2, 1])]
        iitsa_iv = pd.Series([1 / 2, 2 / 2, 2 / 2, 2 / 2])
        iitsa_noiv = pd.Series([1 / 1, 0 / 2, 2 / 2, 2 / 2, 1 / 2])
        plt_path = os.path.join(IMG_PATH, 'fig_phase_1_iitsa_kde.pdf')

        study = awarenessometer.pre_post_study.PrePostStudy(10000, SERIES_INFO, IMG_PATH)
        study._plot_iitsa_kernel_density('pre')

        assert len(mock_plotter.call_args[0][0]) == 2
        assert mock_plotter.call_args[0][0][0].reset_index(drop=True).equals(iitsas[0])
        assert mock_plotter.call_args[0][0][1].reset_index(drop=True).equals(iitsas[1])
        assert mock_plotter.call_args[0][1].reset_index(drop=True).equals(iitsa_iv)
        assert mock_plotter.call_args[0][2].reset_index(drop=True).equals(iitsa_noiv)
        assert mock_plotter.call_args[0][3] == plt_path
        assert mock_plotter.call_args[0][4] == 1

    def test_create_post_kde(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.pre_post_study.Plotter, 'iitsa_kde')

        iitsas = [pd.Series([2, 2, 1 ,1]), pd.Series([1, 0, 2])]
        iitsa_iv = pd.Series([2 / 2, 2 / 2, 1 / 2, 1 / 2])
        iitsa_noiv = pd.Series([1 / 1, 0 / 2, 2 / 2])
        plt_path = os.path.join(IMG_PATH, 'fig_phase_2_iitsa_kde.pdf')

        study = awarenessometer.pre_post_study.PrePostStudy(10000, SERIES_INFO, IMG_PATH)
        study._plot_iitsa_kernel_density('post')

        assert len(mock_plotter.call_args[0][0]) == 2
        assert mock_plotter.call_args[0][0][0].reset_index(drop=True).equals(iitsas[0])
        assert mock_plotter.call_args[0][0][1].reset_index(drop=True).equals(iitsas[1])
        assert mock_plotter.call_args[0][1].reset_index(drop=True).equals(iitsa_iv)
        assert mock_plotter.call_args[0][2].reset_index(drop=True).equals(iitsa_noiv)
        assert mock_plotter.call_args[0][3] == plt_path
        assert mock_plotter.call_args[0][4] == 2
